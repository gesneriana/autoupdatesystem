﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoUpdateClient
{
    /// <summary>
    /// 更新的文件信息,从服务器端读取到的数据,  应该用集合存储 List<updateFileInfo>
    /// </summary>
    public class updateFileInfo
    {
        public int id { get; set; }

        public int AppID { get; set; }

        public string fileName { get; set; }

        /// <summary>
        /// 先获取本机上的文件目录, 创建目录 , 再下载文件, 保存文件
        /// (appdoman.currentdoman.basedirectory + fileRealtiveName).replace(fileName,"")
        /// </summary>
        public string fileRealtiveName { get; set; }

        public string filePath { get; set; }

        public string fileLength { get; set; }

        public string fileHashCode { get; set; }

        public string fileState { get; set; }

        public string descrption { get; set; }
    }
}
