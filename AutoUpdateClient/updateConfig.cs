﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoUpdateClient
{
    /// <summary>
    /// 自动更新的配置文件
    /// </summary>
    public class updateConfig
    {

        /// <summary>
        /// 应用程序名称
        /// </summary>
        public string appName { get; set; }

        /// <summary>
        /// 服务器http地址url, 例如 http://10.8.2.103:23333
        /// </summary>
        public string httpHost { get; set; }

        /// <summary>
        /// 客户端当前版本号, 更新完成之后需要更新版本号, 检查文件hashcode
        /// </summary>
        public int version { get; set; }

        /// <summary>
        /// 程序更新之后安装的根目录, 例如 AppDomain.CurrentDomain.BaseDirectory 或者 F:\program\
        /// </summary>
        public string setupRootPath { get; set; }

        /// <summary>
        /// 升级成功之后回调的应用程序名称,包括路径
        /// </summary>
        public string callbackExePath { get; set; }

        /// <summary>
        /// 回调参数列表
        /// </summary>
        public string callBackParams { get; set; }

        /// <summary>
        /// 0,不回调, 否则回调
        /// </summary>
        public int isCallBack { get; set; }

        /// <summary>
        /// 启动的应用程序是否显示窗口,0 显示
        /// </summary>
        public int isHidden { get; set; }
    }
}