﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AutoUpdateServer.Models
{
    public class FileNamesMap : EntityTypeConfiguration<FileNames>
    {
        /// <summary>
        /// 实体模型映射绑定规则
        /// </summary>
        public FileNamesMap()
        {
            HasKey(t => t.id);

            Property(t => t.fileName).IsRequired().HasMaxLength(300);
            Property(t => t.fileRealtiveName).IsRequired().HasMaxLength(300);
            Property(t => t.filePath).IsRequired().HasMaxLength(300);
            Property(t => t.fileState).IsRequired().HasMaxLength(20);

            ToTable("FileNames");
            Property(t => t.id).HasColumnName("id");
            Property(t => t.AppID).HasColumnName("AppID");
            Property(t => t.fileName).HasColumnName("fileName");
            Property(t => t.fileState).HasColumnName("fileState");
            Property(t => t.descrption).HasColumnName("descrption");
            Property(t => t.filePath).HasColumnName("filePath");
            Property(t => t.fileLength).HasColumnName("fileLength");
            Property(t => t.fileHashCode).HasColumnName("fileHashCode");
        }
    }
}