﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AutoUpdateServer.Models
{
    public class AppNamesMap : EntityTypeConfiguration<AppNames>
    {
        /// <summary>
        /// 实体模型映射绑定规则
        /// </summary>
        public AppNamesMap()
        {
            HasKey(t => t.id);

            Property(t => t.AppName).IsRequired().HasMaxLength(20);
            Property(t => t.AppState).IsRequired().HasMaxLength(20);

            ToTable("AppNames");
            Property(t => t.id).HasColumnName("id");
            Property(t => t.AppName).HasColumnName("AppName");
            Property(t => t.AppState).HasColumnName("AppState");
            Property(t => t.AppVersion).HasColumnName("AppVersion");
            Property(t => t.descrption).HasColumnName("descrption");
        }
    }
}