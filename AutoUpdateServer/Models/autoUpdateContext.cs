﻿using System.Data.Entity;

namespace AutoUpdateServer.Models
{
    /// <summary>
    /// sqlite3 实体数据模型
    /// </summary>
    public partial class autoUpdateContext : DbContext
    {
        static autoUpdateContext()
        {
            Database.SetInitializer<autoUpdateContext>(null);
        }

        public autoUpdateContext():base("Name=AutoUpdateContext")
        {

        }

        /// <summary>
        /// 应用程序版本等信息
        /// </summary>
        public DbSet<AppNames> AppNames { get; set; }

        /// <summary>
        /// 应用程序文件的详细信息
        /// </summary>
        public DbSet<FileNames> FileNames { get; set; }

        /// <summary>
        /// 模型绑定规则
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new AppNamesMap());
            modelBuilder.Configurations.Add(new FileNamesMap());
        }
    }
}