﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoUpdateServer.Models
{
    /// <summary>
    /// 某个应用程序的文件信息
    /// </summary>
    public partial class FileNames
    {
        public int id { get; set; }

        /// <summary>
        /// 外键, appid
        /// </summary>
        public int AppID { get; set; }

        public string fileName { get; set; }

        /// <summary>
        /// 服务器端文件完全限定路径
        /// </summary>
        public string filePath { get; set; }

        /// <summary>
        /// 相对路径, 去除 Server.MapPath("~")+ @"\UpdateFiles"
        /// </summary>
        public string fileRealtiveName { get; set; }

        public long fileLength { get; set; }

        public string fileHashCode { get; set; }

        /// <summary>
        /// 状态, 用于控制单个文件的操作, 更新, 删除, 保持
        /// </summary>
        public string fileState { get; set; }


        public string descrption { get; set; }
    }
}