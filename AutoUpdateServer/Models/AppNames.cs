﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoUpdateServer.Models
{
    /// <summary>
    /// 需要提供自动更新服务的app
    /// 更新规则, 版本号大于上一个版本, AppState= 自动更新
    /// 并且 应用程序文件设置为 更新 状态
    /// </summary>
    public partial class AppNames
    {
        public int id { get; set; }

        /// <summary>
        /// 唯一的应用程序名称
        /// </summary>
        public string AppName { get; set; }

        /// <summary>
        /// 应用程序版本号, 需要手动设置版本号 否则版本号不变,客户端不会自动更新
        /// </summary>
        public int AppVersion { get; set; }

        /// <summary>
        /// 自动更新, 停止更新
        /// </summary>
        public string AppState { get; set; }

        public string descrption { get; set; }
    }
}