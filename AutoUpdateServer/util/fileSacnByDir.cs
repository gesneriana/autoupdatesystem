﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AutoUpdateServer.util
{
    /// <summary>
    /// 根据指定的目录扫描所有文件
    /// </summary>
    public class fileSacnByDir
    {


        public fileSacnByDir(DirectoryInfo dir)
        {
            fileList = new List<FileInfo>();
            GetAllFiles(dir);
        }

        /// <summary>
        /// 扫描的文件列表
        /// </summary>
        public List<FileInfo> fileList { get; set; }


        private void GetAllFiles(DirectoryInfo dir)
        {
            FileInfo[] allFile = dir.GetFiles();
            foreach (FileInfo fi in allFile)
            {
                fileList.Add(fi);
            }
            DirectoryInfo[] allDir = dir.GetDirectories();
            foreach (DirectoryInfo d in allDir)
            {
                GetAllFiles(d);
            }
        }

        /// <summary>
        /// 获取文件的hashcode
        /// </summary>
        /// <param name="fi"></param>
        /// <returns></returns>
        public string fileHashCode(FileInfo fi)
        {
            var hash = System.Security.Cryptography.HashAlgorithm.Create();
            var stream = new FileStream(fi.FullName, FileMode.Open);
            var bts = hash.ComputeHash(stream);
            stream.Close();
            var str = BitConverter.ToString(bts);
            return str;
        }

    }

}
